<?php

/*
 * Yampee Components
 * Open source web development components for PHP 5.
 *
 * @package Yampee Components
 * @author Titouan Galopin <galopintitouan@gmail.com>
 * @link http://titouangalopin.com
 */

namespace Ox\Components\Yampee\Tests\Unit\Redis;

use Ox\Components\Yampee\Redis\Client;
use PHPUnit\Framework\TestCase;

/**
 * Test a Redis connection.
 */
class ClientTest extends TestCase
{
    public function testConnection()
    {
        $redis = new Client();
        $redis->connect();

        $redis->set('key', 'value');

        $this->assertTrue($redis->has('key'));
        $this->assertEquals('value', $redis->get('key'));

        $redis->remove('key');

        $this->assertFalse($redis->has('key'));
    }

    public function testList()
    {
        $redis = new Client();
        $redis->connect();

        $redis->listPush('mylist', 'index0');
        $redis->listPush('mylist', 'index1');
        $redis->listPush('mylist', 'index2');

        $this->assertEquals('index0', $redis->listGet('mylist', 0));
        $this->assertEquals('index1', $redis->listGet('mylist', 1));
        $this->assertEquals('index2', $redis->listGet('mylist', 2));

        $this->assertTrue($redis->has('mylist'));

        $redis->remove('mylist');

        $this->assertFalse($redis->has('mylist'));
    }

    public function testGetWithUnknownKeyDoesNotThrowAnException(): void
    {
        $redis = new Client();
        $redis->connect();

        $should_be_null = $redis->get(uniqid());

        $this->assertNull($should_be_null);
    }
}