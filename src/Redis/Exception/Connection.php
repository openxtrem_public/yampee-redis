<?php

/**
 * Yampee Components
 * Open source web development components for PHP 5.
 *
 * @package Yampee Components
 * @author  Titouan Galopin <galopintitouan@gmail.com>
 * @link    http://titouangalopin.com
 */

namespace Ox\Components\Yampee\Redis\Exception;

use Exception;

/**
 * Connection exception.
 */
class Connection extends Exception
{
    /** @var string */
    protected $host;

    /** @var int */
    protected $port;

    /**
     * Connection constructor.
     *
     * @param string         $host
     * @param int            $port
     * @param string|null    $message
     */
    public function __construct($host, $port, $message = null)
    {
        parent::__construct();

        $this->host    = $host;
        $this->port    = $port;
        $this->message = $message ?: sprintf('Unable to connect to Redis at "%s:%s".', $host, $port);
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }
}