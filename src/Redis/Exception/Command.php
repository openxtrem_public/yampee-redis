<?php

/**
 * Yampee Components
 * Open source web development components for PHP 5.
 *
 * @package Yampee Components
 * @author  Titouan Galopin <galopintitouan@gmail.com>
 * @link    http://titouangalopin.com
 */

namespace Ox\Components\Yampee\Redis\Exception;

use Exception;

/**
 * Command exception.
 */
class Command extends Exception
{
    /** @var string */
    protected $command;

    /**
     * Command constructor.
     *
     * @param string $command
     */
    public function __construct($command)
    {
        parent::__construct();

        $this->command = $command;
        $this->message = sprintf('Unable to execute command "%s".', $command);
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }
}