<?php

/**
 * Yampee Components
 * Open source web development components for PHP 5.
 *
 * @package Yampee Components
 * @author  Titouan Galopin <galopintitouan@gmail.com>
 * @link    http://titouangalopin.com
 */

namespace Ox\Components\Yampee\Redis\Exception;

use Exception;

/**
 * Redis error.
 */
class Error extends Exception
{
    /** @var string */
    protected $redisError;

    /**
     * Error constructor.
     *
     * @param string $redisError
     */
    public function __construct($redisError)
    {
        parent::__construct();

        $this->redisError = $redisError;
        $this->message    = sprintf('Redis error: "%s".', $redisError);
    }

    /**
     * @return string
     */
    public function getRedisError()
    {
        return $this->redisError;
    }
}