<?php

/**
 * Yampee Components
 * Open source web development components for PHP 5.
 *
 * @package Yampee Components
 * @author  Titouan Galopin <galopintitouan@gmail.com>
 * @link    http://titouangalopin.com
 */

namespace Ox\Components\Yampee\Redis;

use Ox\Components\Yampee\Redis\Exception\Command as CommandException;
use Ox\Components\Yampee\Redis\Exception\Connection as ConnectionException;
use Ox\Components\Yampee\Redis\Exception\Error as ErrorException;
use Ox\Components\Yampee\Redis\Exception\ReadReply as ReadReplyException;

/**
 * Implements a Redis client for PHP 5.2.
 */
class Client
{
    const LIST_PUSH_RIGHT = 10;
    const LIST_PUSH_LEFT  = 20;

    const LIST_POP_RIGHT = 10;
    const LIST_POP_LEFT  = 20;

    /** @var Connection */
    protected $connection;

    /** @var string */
    protected $host = 'localhost';

    /** @var int */
    protected $port = 6379;

    /**
     * Client constructor.
     *
     * @param string $host
     * @param int    $port
     */
    public function __construct($host = 'localhost', $port = 6379)
    {
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * Connect (or reconnect) to Redis with given parameters
     *
     * @param float|int $timeout
     *
     * @return $this
     * @throws ConnectionException
     */
    public function connect(float $timeout = 5): Client
    {
        $this->connection = new Connection($this->host, $this->port, $timeout);

        return $this;
    }

    /**
     * Close connection
     *
     * @return void
     */
    public function close(): void
    {
        $this->connection = null;
    }

    /**
     * Check whether client is connected or not.
     *
     * @return bool
     */
    public function isConnected(): bool
    {
        return !!$this->connection;
    }

    /**
     * Get a value by its key.
     *
     * @param string $key
     *
     * @return array|mixed|null
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function get($key)
    {
        return $this->send('get', [$key]);
    }

    /**
     * Check if the given key exists in the database.
     *
     * @param string $key
     *
     * @return bool
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function has($key): bool
    {
        return (bool)$this->send('exists', [$key]);
    }

    /**
     * Set a value and its key.
     *
     * @param string $key
     * @param mixed  $value
     * @param null   $expire
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function set($key, $value, $expire = null)
    {
        if (is_int($expire)) {
            return $this->send('setex', [$key, $expire, $value]);
        } else {
            return $this->send('set', [$key, $value]);
        }
    }

    /**
     * Add a value in a list.
     *
     * @param string $listName
     * @param mixed  $value
     * @param int    $pushType
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function listPush($listName, $value, $pushType = self::LIST_PUSH_RIGHT)
    {
        $command = 'rpush';

        if ($pushType == self::LIST_PUSH_LEFT) {
            $command = 'lpush';
        }

        return $this->send($command, [$listName, $value]);
    }

    /**
     * Remove the first or the last value from a list.
     *
     * @param string $listName
     * @param int    $popType
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function listPop($listName, $popType = self::LIST_POP_RIGHT)
    {
        $command = 'rpop';

        if ($popType == self::LIST_POP_LEFT) {
            $command = 'lpop';
        }

        return $this->send($command, [$listName]);
    }

    /**
     * Get an element from a list by its index
     *
     * @param string $listName
     * @param int    $index
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function listGet($listName, $index)
    {
        return $this->send('lindex', [$listName, $index]);
    }

    /**
     * Get an element from a hash by its key
     *
     * @param $hashName
     * @param $key
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function hashGet($hashName, $key)
    {
        return $this->send('hget', [$hashName, $key]);
    }

    /**
     * Set an element on a hash by its key
     *
     * @param $hashName
     * @param $key
     * @param $value
     *
     * @return bool
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function hashSet($hashName, $key, $value)
    {
        return (bool)$this->send('hset', [$hashName, $key, $value]);
    }

    /**
     * Delete an element from a hash by its key
     *
     * @param $hashName
     * @param $key
     *
     * @return bool
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function hashDelete($hashName, $key)
    {
        return (bool)$this->send('hdel', [$hashName, $key]);
    }

    /**
     * Set an element from a list by its index
     *
     * @param string $listName
     * @param int    $index
     * @param mixed  $value
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function listSet($listName, $index, $value)
    {
        return $this->send('lset', [$listName, $index, $value]);
    }

    /**
     * Get a range of elements from a list.
     *
     * @param string $listName
     * @param int    $firstIndex
     * @param int    $lastIndex
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function listGetRange($listName, $firstIndex, $lastIndex)
    {
        return $this->send('lrange', [$listName, $firstIndex, $lastIndex]);
    }

    /**
     * Get a list length.
     *
     * @param string $listName
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function listLength($listName)
    {
        return $this->send('llen', [$listName]);
    }

    /**
     * Delete a key and its value from the database.
     *
     * @param string $key
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function remove($key)
    {
        return $this->send('del', [$key]);
    }

    /**
     * Try to authenticate the user using the given password to the Redis server.
     *
     * @param string $password
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function authenticate($password)
    {
        return $this->send('auth', [$password]);
    }

    /**
     * Remove the expiration from a key.
     *
     * @param string $key
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function persist($key)
    {
        return $this->send('persist', [$key]);
    }

    /**
     * Find all the keys matching the pattern.
     * See more about the pattern on Redis documentation:
     * @link http://redis.io/commands/keys
     *
     * @param string $pattern
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function findKeys($pattern = '*')
    {
        return $this->send('keys', [$pattern]);
    }

    /**
     * Delete all the keys of the currently selected database.
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function flush()
    {
        return $this->send('flushdb');
    }

    /**
     * Get information and statistics about the Redis server.
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function getStats()
    {
        return $this->send('info');
    }

    /**
     * Get a config element value by its name.
     *
     * @param string $parameterName
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function getParameter($parameterName)
    {
        return $this->send('config', ['GET', $parameterName]);
    }

    /**
     * Set a config element value by its name.
     *
     * @param string $parameterName
     * @param mixed  $value
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function setParameter($parameterName, $value)
    {
        return $this->send('config', ['SET', $parameterName, $value]);
    }

    /**
     * Get the Redis database size.
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function getSize()
    {
        return $this->send('dbsize');
    }

    /**
     * Send a command to Redis and return the reply.
     *
     * @param string $command
     * @param array  $arguments
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function send($command, array $arguments = [])
    {
        return $this->execute(array_merge([$command], $arguments));
    }

    /**
     * Execute a command with Redis and return the result.
     *
     * @param array $arguments
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    protected function execute(array $arguments)
    {
        // Try to connect
        if (!$this->connection) {
            $this->connect();
        }

        // Create the command
        $command = '*' . count($arguments) . "\r\n";

        foreach ($arguments as $argument) {
            $command .= '$' . strlen($argument) . "\r\n" . $argument . "\r\n";
        }

        // Send the command
        if (!$this->connection->send($command)) {
            // If an error occurred during first sending, we try to reconnect
            $this->connect();

            if (!$this->connection->send($command)) {
                throw new CommandException($command);
            }
        }

        return $this->readReply($command);
    }

    /**
     * Read a Redis reply.
     *
     * @param string $command
     *
     * @return mixed
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    protected function readReply($command)
    {
        $reply = $this->connection->read();

        // If an error occurred during first sending, we try to reconnect
        if ($reply === false) {
            $this->connect();

            $reply = $this->connection->read();

            if ($reply === false) {
                throw new ReadReplyException($command);
            }
        }

        $reply = trim($reply);

        switch ($reply[0]) {
            // An error occurred
            case '-':
                throw new ErrorException($reply);

            // Inline response
            case '+':
                return substr($reply, 1);

            // Bulk response
            case '$':
                $response = null;

                if ($reply == '$-1') {
                    // $-1 is a Null Bulk String
                    return null;
                }

                $size = intval(substr($reply, 1));

                if ($size > 0) {
                    $response = stream_get_contents($this->connection->getSocket(), $size);
                }

                // Discard CRLF
                $this->connection->positionRead(2);

                return $response;

            // Multi-bulk response
            case '*':
                $count = substr($reply, 1);

                if ($count == '-1') {
                    return null;
                }

                $response = [];

                for ($i = 0; $i < $count; $i++) {
                    $response[] = $this->readReply($command);
                }

                return $response;

            // Integer response
            case ':':
                return intval(substr($reply, 1));

            // Error: not supported
            default:
                throw new ErrorException('Non-protocol answer: ' . print_r($reply, 1));
        }
    }

    /**
     * Set a value without overwriting if it already exists.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return mixed
     *
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function setNX($key, $value)
    {
        return $this->send("SETNX", [$key, $value]);
    }

    /**
     * Renames key to new key if new key does not yet exist.
     * It returns an error under the same conditions as RENAME.
     *
     * @param string $key
     * @param string $new_key
     *
     * @return mixed
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function renameNX($key, $new_key)
    {
        return $this->send("RENAMENX", [$key, $new_key]);
    }

    /**
     * Set a timeout on key. After the timeout has expired, the key will automatically be deleted.
     *
     * @param string $key
     * @param float  $seconds
     *
     * @return int 1 or 0
     *
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function expire($key, $seconds)
    {
        return $this->send("EXPIRE", [$key, $seconds]);
    }

    /**
     * Atomic get / set
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return mixed
     *
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function getSet($key, $value)
    {
        return $this->send("GETSET", [$key, $value]);
    }

    /**
     * Start a transaction
     *
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function multi(): void
    {
        $this->send("MULTI");
    }

    /**
     * Exec a transaction, atomically
     *
     * @return array An array of the results
     *
     * @throws CommandException
     * @throws ConnectionException
     * @throws ErrorException
     * @throws ReadReplyException
     */
    public function exec()
    {
        return $this->send("EXEC");
    }
}
