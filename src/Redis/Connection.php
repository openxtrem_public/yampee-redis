<?php

/**
 * Yampee Components
 * Open source web development components for PHP 5.
 *
 * @package Yampee Components
 * @author  Titouan Galopin <galopintitouan@gmail.com>
 * @link    http://titouangalopin.com
 */

namespace Ox\Components\Yampee\Redis;

use Ox\Components\Yampee\Redis\Exception\Connection as ConnectionException;

/**
 * Represents a Redis connection.
 */
class Connection
{
    /** @var resource */
    protected $socket;

    /**
     * Connection constructor.
     *
     * @param string    $host
     * @param int       $port
     * @param float|int $timeout
     *
     * @throws ConnectionException
     */
    public function __construct($host = 'localhost', $port = 6379, float $timeout = 5)
    {
        $socket = fsockopen($host, $port, $errno, $errstr, $timeout);

        if (!$socket) {
            throw new ConnectionException($host, $port, $errstr);
        }

        $this->socket = $socket;
    }

    /**
     * @param string $command
     *
     * @return int
     */
    public function send($command)
    {
        return fwrite($this->socket, $command);
    }

    /**
     * @return string
     */
    public function read()
    {
        return fgets($this->socket);
    }

    /**
     * @param int $position
     *
     * @return string
     */
    public function positionRead($position)
    {
        return fread($this->socket, $position);
    }

    /**
     * @return resource
     */
    public function getSocket()
    {
        return $this->socket;
    }
}